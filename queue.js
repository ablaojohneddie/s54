let collection = [];

// Write the queue functions below.
function print(){
    return collection;
}

function enqueue(name){
    if(collection.length == 0){
        collection[0] = name;
    } else {
        collection[collection.length] = name;
    }
    return collection;
}

function dequeue(){
    let dummy = [];
    for(let i = 0; i < collection.length - 1; i++){
        dummy[i] = collection[i+1];
    }
    return collection = [...new Set(dummy)];
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    if(collection.length > 0){
        return false;
    } else {
        return true;
    }
}



module.exports = { 
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty  
}; 